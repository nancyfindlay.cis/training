<?php
include_once("../conn/db.php");
// catid, catimg, catname, cataddeaddate, catmodifydate, catorder, catstatus
if (isset($_POST['status']) && $_POST['status'] != "") {
    $status = $_POST['status'];
    if ($status == "Active") {
        $query = "SELECT catid, catimg, catname, cataddeaddate, catmodifydate, catorder, catstatus  
                  FROM category 
                  WHERE CatStatus ='$status' 
                  ORDER BY catorder ASC";
    } else if ($status == "Inactive") {
        $query = "SELECT catid, catimg, catname, cataddeaddate, catmodifydate, catorder, catstatus
                  FROM category 
                  WHERE CatStatus ='$status'  
                  ORDER BY catorder ASC";
    } else {
        $query = "SELECT catid, catimg, catname, cataddeaddate, catmodifydate, catorder, catstatus
                  FROM category 
                  ORDER BY catorder ASC";
    }
} else {

    $search = $_POST['searchcat'];
    echo $_POST['searchcat'];

    if ($search == "") {
        $query = "SELECT catid, catimg, catname, cataddeaddate, catmodifydate, catorder, catstatus
            FROM category ORDER BY catorder ASC";
    } else {
        $query = "SELECT catid, catimg, catname, cataddeaddate, catmodifydate, catorder, catstatus
            FROM category WHERE CatName LIKE '%$search%'";
    }
}

$path = "../img/category/";
$res = mysqli_query($conn, $query);

if (mysqli_num_rows($res) > 0) {
    // catid, catimg, catname, cataddeaddate, catmodifydate, catorder, catstatus
    foreach ($res as $key => $value) {

        $id  = $value["catid"];
        $noOfProduct = "SELECT COUNT(prodid) AS noOfProduct FROM product 
                        WHERE prodstatus = 'Active' AND catid = $id";
        $resOfProduct = mysqli_query($conn, $noOfProduct) or die("not find the no of product");
        $rowOfProduct = mysqli_fetch_assoc($resOfProduct);
        echo "<tr>";
        echo "<td> <img src=" . $path . $value['catimg'] . " width =70 height = 70></td>";
        echo "<td>" . $value['catname'] . "</td>";
        echo "<td>" . $value['catorder'] . "</td>";
        echo "<td>" . $rowOfProduct['noOfProduct'] . "</td>";
        echo "<td>" . $value['catstatus'] . "</td>";
        echo "<td> <a href=../category/cat_fetch.php?id=" . $value['catid'] . " class='btn btn-outline-danger' > Edit </a> </td>";
        echo "<td><button type='button' class='btn btn-outline-danger' onclick='CatDelete(" . $value['catid'] . ")'>Delete</button></td>";
        echo "</tr>";
    }
}
