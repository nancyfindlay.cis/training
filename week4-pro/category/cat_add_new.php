<?php
include_once("../conn/db.php");
session_start();
if ($_SESSION['email'] == null && $_SESSION['email'] == "") {
    header("location:../login/login.php");
}
$filename =  $_FILES['imgpath']['name'];
$tempname = $_FILES['imgpath']['tmp_name'];
$catname = trim($_POST['catname']);

$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
$filename = time() + mt_rand(100, 1000) . '.' . $ext;
$order =  $_POST['order'];
$catstatus =  $_POST['catstatus'];
$folder = "../img/category/" . $filename;

// catid, catimg, catname, catorder, catstatus
$catInsert = "INSERT INTO category (catimg, catname, catorder,catstatus) 
        VALUES ('$filename','$catname', $order,'$catstatus')";
$catRes = mysqli_query($conn, $catInsert) or die("something is wrong");

if ($catRes) {
    if (move_uploaded_file($tempname, $folder)) {
        header("Location:../category/category.php");
    } else {
        $msg = "Failed to upload image";
        echo "<script>";
        echo "alert('something is wrong {$msg}')";
        echo "</script>";
    }
} else {
    echo "<script>";
    echo "alert('Record is not inseted')";
    echo "</script>";
}
