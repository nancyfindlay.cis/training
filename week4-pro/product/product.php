<?php
session_start();
if ($_SESSION['email'] == null && $_SESSION['email'] == "") {
    header("location:../login/login.php");
}
include_once("../conn/db.php");
include_once("../header/header.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product Section</title>
    <script src="../js/sweetalert.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css"></script>
    <link rel="stylesheet" href="../css/custome.css">

</head>

<body>
    <div class="container">
        <h2 class="text-center">All Product Record</h2>
        <div class="form-row">
            <div class="col">
                <a href="prod_add.php" style="margin-bottom: 10px" class="btn btn-outline-primary">Add New Product</a>
            </div>
            <div class="col-8">
                <input type="text" name="searchpro" id="searchpro" class="form-control" autofocus>
            </div>
            <div class="col">
                <select class="btn btn-outline-danger float-right" name="prodstatus" id="prodstatus">
                    <option selected>all</option>
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                </select>
            </div>
            <div class="form-row align-items-center">
                <div class="col-auto">
                    <input type="text" class="form-control mb-2" id="minValuePrice" name="minValuePrice" placeholder="Enter Min value of price">
                </div>
                <div class="col-auto">
                    <div class="input-group mb-2">
                        <input type="text" class="form-control" id="maxValuePrice" name="maxValuePrice" placeholder="Enter Max value of price">
                    </div>
                </div>
                <div class="col-auto">
                    <button type="submit" name="minMaxPrice" id="minMaxPrice" class="btn btn-primary mb-2">Filter Price</button>
                </div>
            </div>
            <div class="form-row align-items-center">
                <div class="col-auto">
                    <input type="text" class="form-control mb-2 ml-2" name="minQuantity" id="minQuantity" placeholder="Enter Min quantity">
                </div>
                <div class="col-auto">
                    <div class="input-group mb-2">
                        <input type="text" class="form-control" name="maxQuantity" id="maxQuantity" placeholder="Enter Max quantity">
                    </div>
                </div>
                <div class="col-auto">
                    <button type="submit" name="minMaxQuantity" id="minMaxQuantity" class="btn btn-primary mb-2">Filter Qty</button>
                </div>
                <div class="col-auto">
                    <button type="submit" name="resetAll" id="resetAll" class="btn btn-primary mb-2 float-right">reset</button>
                </div>
            </div>
        </div>
        <table id="example" class="table table-bordered display">
            <thead>
                <tr>
                    <th>Product Img</th>
                    <th>Pro Name</th>
                    <th>Pro Code</th>
                    <th>Cat Name</th>
                    <th>Pro Price</th>
                    <th>Pro Qty</th>
                    <th>Pro Order</th>
                    <th>Pro status</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody id="data_table">
                <?php
                $sql = "SELECT pi.p_img, p.prodid,p.prodname, p.catid,p.prodcode, p.prodstatus, 
                p.prodprice, p.prodqty, p.prodcreatedate, p.prodmodifydate, p.prodorder, pi.p_img, c.catname 
                FROM product p 
                INNER JOIN p_image pi ON pi.prodid = p.prodid 
                LEFT JOIN category c ON c.catid = p.catid 
                WHERE pi.p_img_status = 1 AND c.catstatus = 'Active'
                ORDER BY prodorder ASC";

                $res = mysqli_query($conn, $sql) or die("query not exicute" + mysqli_errno($conn));
                if (mysqli_num_rows($res) > 0) {
                ?>
                    <?php
                    while ($row = mysqli_fetch_assoc($res)) {
                    ?>
                        <tr>

                            <td> <img src="../img/product/<?php echo $row['p_img'] ?>" width="50px" height="50px" alt="notdisplay"> </td>
                            <td><?php echo $row['prodname']; ?></td>
                            <td><?php echo $row['prodcode']; ?> </td>
                            <td><?php echo $row['catname']; ?> </td>
                            <td><?php echo $row['prodprice']; ?></td>
                            <td><?php echo $row['prodqty']; ?></td>
                            <td> <?php echo $row['prodorder'] ?></td>
                            <td><?php if ($row['prodstatus'] == "Active") {
                                    echo '<button class="btn btn-primary" onclick = ActiveInActive(' . $row["prodid"] . '); >Active</button>';
                                } else {
                                    echo '<button class="btn btn-danger" onclick = ActiveInActive(' . $row["prodid"] . ');>InActive</button>';
                                }  ?></td>

                            <td><a class="btn btn-outline-danger" href="../product/prod_fetchbyid.php?id=<?php echo $row['prodid']; ?>">Edit</a></td>

                            <td><button class="btn btn-outline-danger" id="btndelete" onclick="ProdDelete(<?php echo $row['prodid']; ?>)" ?>Delete</button></td>

                        </tr>
                <?php }
                } else {
                    echo "<script>";
                    echo "alert('No any record please add record')";
                    echo "</script>";
                }
                ?>
            </tbody>
        </table>
    </div>

    <script>
        var SearchP = document.getElementById("searchpro");
        SearchP.addEventListener("keydown", function(e) {
            if (e.keyCode === 13) {
                SearchProduct();
            }
        });

        function SearchProduct() {
            $("#prodstatus")[0].selectedIndex = 0
            $("#minQuantity").val("");
            $("#maxQuantity").val("");
            $("#minValuePrice").val("");
            $("#maxValuePrice").val("");
            var searchpro = $('#searchpro').val();
            $.ajax({
                url: "p_search.php",
                method: "POST",
                data: {
                    searchpro: searchpro
                },
                success: function(data) {
                    $("tbody").html(data);
                }
            });
        }

        function ProdDelete(del) {
            swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this record!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        var id = del;
                        $.ajax({
                            type: "POST",
                            url: "p_delete.php",
                            data: {
                                id: id
                            },
                            success: function(value) {
                                $("#data_table").html(value);
                            }
                        });
                        swal("Your record has been deleted!", {
                            icon: "success",
                        });
                    } else {
                        swal("Your record is not deleted!", {
                            icon: "error",
                        });
                    }
                })
        }

        function ActiveInActive(id) {
            var id = id;
            $.ajax({
                type: "POST",
                url: "p_active_inactive.php",
                data: {
                    id: id
                },
                success: function(value) {
                    $("#data_table").html(value);
                }
            });
        }

        $(document).ready(function() {
            //start product search by status
            $('#prodstatus').change(function() {
                var status = $('#prodstatus').val();
                $("#minQuantity").val("");
                $("#maxQuantity").val("");
                $("#minValuePrice").val("");
                $("#maxValuePrice").val("");
                $("#searchpro").val("");
                $.ajax({
                    url: "p_search.php",
                    method: "POST",
                    data: {
                        status: status
                    },
                    success: function(data) {
                        $("tbody").html(data);
                    }
                });
            });
            //end product status 


        });

        //start fileter the price min and max
        $("#minMaxPrice").click(function() {
            $("#minQuantity").val("");
            $("#maxQuantity").val("");
            $("#searchpro").val("");
            $("#prodstatus")[0].selectedIndex = 0
            let minPrice = $("#minValuePrice").val();
            let maxPrice = $("#maxValuePrice").val();
            if (minPrice != "" && maxPrice != "") {

                $.ajax({
                    url: "p_search.php",
                    method: "POST",
                    data: {
                        minPrice: minPrice,
                        maxPrice: maxPrice
                    },
                    success: function(data) {
                        $("tbody").html(data);
                    }
                });
            } else {
                alert("Enter the max or min value ");
            }
        });


        $("#minMaxQuantity").click(function() {

            $("#minValuePrice").val("");
            $("#maxValuePrice").val("");
            $("#searchpro").val("");
            let minQuantity = $("#minQuantity").val();
            let maxQuantity = $("#maxQuantity").val();
            if (minQuantity != "" && maxQuantity != "") {

                $.ajax({
                    url: "p_search.php",
                    method: "POST",
                    data: {
                        minQuantity: minQuantity,
                        maxQuantity: maxQuantity
                    },
                    success: function(data) {
                        $("tbody").html(data);
                    }
                });
            } else {
                alert("Please Enter the min or max quantity");
            }

        });


        $("#resetAll").click(function() {
            $("#minQuantity").val("");
            $("#maxQuantity").val("");
            $("#minValuePrice").val("");
            $("#maxValuePrice").val("");
            $("#searchpro").val("");
            $.ajax({
                url: "p_search.php",
                method: "POST",
                data: {},
                success: function(data) {
                    $("tbody").html(data);
                }
            });
        });
        $(document).ready(function() {
            $('#example').DataTable({
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": [0, 1, 2, 3, 6, 7, 8, 9]
                }]
            });
        });
    </script>
    </div>
</body>

</html>