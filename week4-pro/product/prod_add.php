<?php
session_start();
if ($_SESSION['email'] == null && $_SESSION['email'] == "") {
    header("location:../login/login.php");
}
include_once("../conn/db.php");
include_once("../header/header.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>add new Product</title>
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="../css/custome.css" href="">
</head>
<body>
    <h1 class="text-center">Add new Product</h1>
    <form action="prod_add_new.php" method="POST" id ="prodadd" enctype="multipart/form-data">
        <div class="container">
        <!-- proid,catid,prodname,prodcode,prodstatus,prodprice,prodqty,prodcreatedate,prodmodifydate,prodorder  -->
            <div class="form-group">
                <label for="prodname">Product Name</label>
                <input type="text" class="form-control form-rounded" name="prodname" id="prodname" autofocus  required>
            </div>

            <div class="form-group">
                <label for="prodcode">Product Code</label>
                <input type="text" class="form-control form-rounded"  value="<?php echo time().mt_rand(1,1000);?>" name="prodcode" id="prodcode" readonly required>
            </div>

            <div class="form-group">
                <label for="prodprice">Product price</label>
                <input type="number" class="form-control form-rounded" name="prodprice" id="prodprice" required>
            </div>

            <div class="form-group">
                <label for="prodqty">Product Quantity</label>
                <input type="number" class="form-control form-rounded" name="prodqty" id="prodqty" required>
            </div>

            <div class="form-group">
                <label for="prodqty">select category </label>
                <?php
                $sql = "SELECT catid, catname FROM category WHERE catstatus = 'Active'";
                $res = mysqli_query($conn, $sql);
                echo '<select class="form-control form-rounded" name="catid">';
                foreach ($res as $key => $value) {    
                    echo '<option value = '.$value["catid"].' > '.$value["catname"].' </option>';
                }
                echo '</select>';
                ?>
            </div>

            <div class="form-group">
                <label for="prodorder">Product order</label>
                <select class="form-control form-rounded" name="prodorder" id="prodorder" required>
                    <option value="1">Bat</option>
                    <option value="2">Ball</option>
                    <option value="3">Hat</option>
                    <option value="4">Stump</option>
                </select>
            </div>

            <div class="form-group">
                <label for="prodstatus">Product status:</label>
                <input type="radio" value="Active" name="prodstatus" id="active" checked>
                <label for="Active">Active</label>
                <input type="radio" value="Inactive" name="prodstatus" id="inactive">
                <label for="Inactive">Inactive</label>
            </div>
            <div class="form-group">
                <label for="fileUpload">Upload multiple image</label>
                <input type="file" accept="image/*" class="form-control form-rounded" name="fileUpload[]" id="fileUpload" required multiple>
            </div>
            <input type="submit" value="Add Product" name="submit" id="submit" class="btn btn-primary form-rounded">
            <input type="reset" value="Reset" class="btn btn-warning form-rounded">
        </div>
    </form>
</body>


</html>