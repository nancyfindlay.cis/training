<?php
//proid,catid,prodname,prodcode,prodstatus,prodprice,prodqty,prodcreatedate,prodmodifydate,prodorder 
include_once("../conn/db.php");
$id = $_POST['id'];

$sqlProdStatusUpdate = "UPDATE product SET prodstatus = (CASE WHEN prodstatus = 'Active' THEN 'Inactive' ELSE 'Active' END) WHERE prodid = '{$id}'";
$resProdUpdated = mysqli_query($conn, $sqlProdStatusUpdate) or die("something is wrong");

if ($resProdUpdated) {
    $sqlProdSelect = "SELECT pi.p_img, p.prodid,p.prodname, p.catid,p.prodcode, p.prodstatus, p.prodprice, 
                      p.prodqty, p.prodcreatedate,p.prodmodifydate,p.prodorder, pi.p_img, c.catname 
                      FROM product p 
                      INNER JOIN p_image pi ON pi.prodid = p.prodid 
                      LEFT JOIN category c ON c.catid = p.catid 
                      WHERE pi.p_img_status = 1 AND c.catstatus = 'Active'
                      ORDER BY prodorder ASC";
    $resProdSelect =  mysqli_query($conn, $sqlProdSelect);
    $path = "../img/product/";
    if (mysqli_num_rows($resProdSelect)) {
        foreach ($resProdSelect as $key => $value) {
            echo "<tr>";
            echo "<td> <img src=" . $path . $value['p_img'] . " width =70 height = 70></td>";
            echo "<td>" . $value['prodname'] . "</td>";
            echo "<td>" . $value['prodcode'] . "</td>";
            echo "<td>" . $value['catname'] . "</td>";
            echo "<td>" . $value['prodprice'] . "</td>";
            echo "<td>" . $value['prodqty'] . "</td>";
            echo "<td>" . $value['prodorder'] . "</td>";
            if ($value['prodstatus'] == "Active") {
                echo '<td><button class="btn btn-primary" onclick = ActiveInActive('.$value["prodid"].'); >Active</button></td>';
            } else {
                echo '<td><button class="btn btn-danger" onclick = ActiveInActive('.$value["prodid"].');>InActive</button></td>';
            }
            echo "<td> <a href=prod_fetchbyid.php?id=" . $value['prodid'] . " class='btn btn-outline-danger' > Edit </a> </td>";
            echo "<td><button type='button' class='btn btn-outline-danger' onclick='ProdDelete(" . $value['prodid'] . ")'>Delete</button></td>";
            echo "</tr>";
        }
    }
} else {
    echo "somethings is wrong";
}
