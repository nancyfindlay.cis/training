-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2020 at 08:44 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `studentinfo`
--

-- --------------------------------------------------------

--
-- Table structure for table `stdregistration`
--

CREATE TABLE `stdregistration` (
  `id` int(10) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phonenumber` text NOT NULL,
  `addres` varchar(300) NOT NULL,
  `gender` int(10) NOT NULL,
  `courses` int(10) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stdregistration`
--

INSERT INTO `stdregistration` (`id`, `fname`, `email`, `phonenumber`, `addres`, `gender`, `courses`, `timestamp`) VALUES
(16, 'kanzariya hardik', 'hardikkanzariya@gmail.com', '7046130838', 'hardik kanz', 2, 2, '2020-12-22 06:44:41'),
(17, 'jayraj', 'jayrajchaya123@gmail.com', '707070070', 'at-jamn', 3, 2, '2020-12-22 06:51:01'),
(18, 'kanzariya hardik', 'hardikkanzariya@gmail.com', '7046130838', 'hardik kanzariya', 2, 2, '2020-12-22 06:57:29'),
(19, 'harbert', 'herbertjion@gmail.com', '707070', 'herbertzion botad', 1, 3, '2020-12-22 07:35:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `stdregistration`
--
ALTER TABLE `stdregistration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `stdregistration`
--
ALTER TABLE `stdregistration`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
