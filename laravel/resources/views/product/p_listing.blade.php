@extends('dashboard')
@section('content')
@if(Session::get('success'))
<div class="alert alert-success" role="alert" id="success-alert">
    <strong>{{Session::get('success')}} </strong>
</div>
@endif
@if(Session::get('danger'))
<div class="alert alert-danger" role="alert">
    <strong>{{Session::get('danger')}} </strong>
</div>
@endif
<div class="form-row">
    <div class="form-group col-md-6">
        <a type="button" class="btn btn-primary " href="{{route('addNewProduct')}}">Add New Product</a>
    </div>
</div>
<form action="prodSearch" method="post">
    {{csrf_field()}}
    <div class="form-row">
        <div class="form-group">
            <input type="text" class="form-control row" value="" placeholder="Minimum Product Price" name="prodPriceMin" id="prodPriceMin">
        </div>
        <div class="form-group">
            <input type="text" class="form-control row mx-2" value="" placeholder="Maximum Product Price" name="prodPriceMax" id="prodPriceMax">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary row mx-3" value="filter Price"></input>
        </div>
        <div class="form-group">
            <input type="text" class="form-control row" value="" placeholder="Minimum Product Price" name="prodQtyMin" id="prodQtyMin">
        </div>
        <div class="form-group">
            <input type="text" class="form-control row mx-2" value="" placeholder="Minimum Product Price" name="prodQtyMax" id="prodQtyMax">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary row mx-3" value="filter Qty"></input>
        </div>
        <div class="form-group col-md-5">
            <input type="text" class="form-control" value="" placeholder="Search Product" name="searchProd" id="searchProd">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary row mx-3" value="Search Product"></input>
        </div>
    </div>
</form>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Id</th>
            <th>Product Name</th>
            <th>category Name</th>
            <th>Prod image</th>
            <th>Prod Code</th>
            <th>Product price</th>
            <th>Products price</th>
            <th>Produc tqty</th>
            <th>Product order</th>
            <th>Product status</th>
            <th>Create Date</th>
            <th>Update Date</th>
            <th colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($prodRecord as $key=>$value)
        <tr>
            <td>{{$value->id}}</td>
            <td>{{$value->pname}}</td>
            <td>{{$value->c_name}}</td>
            <td><img src="{{url('/asset/img/product/'.$value->p_img)}}" width="70px" height="70px"></td>
            <td>{{$value->p_code}}</td>
            <td>{{$value->p_price}}</td>
            <td>{{$value->p_saleprice}}</td>
            <td>{{$value->p_quantity}}</td>
            <td>{{$value->p_order}}</td>
            <td>{{$value->status}}</td>
            <td>{{date('d-m-Y', strtotime($value->created_at))}}</td>
            <td>{{date('d-m-Y', strtotime($value->updated_at))}}</td>
            <td><a href="prodEdit/{{$value->id}}" class="btn btn-success">Edit</a>
                <button onclick="prodDelete({{$value->id}})" class="btn btn-danger">Delete</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<script src="{{asset('asset/js/jquery.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $(".alert").fadeTo(2000, 2000).slideUp(2000, function() {
            $(".alert").slideUp(5000);
        });
    });
</script>

<script>
    function prodDelete(id) {
        let a = confirm("Are You Sure wan't to delete This Record");
        if (a == true) {
            $.ajax({
                type: "get",
                url: "prodDelete/" + id,
                success: function(response) {
                    location.reload();
                }
            });
        } else {
            console.log("record is safe");
        }
    }
</script>
@endsection