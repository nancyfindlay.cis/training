@extends('dashboard')
@section('content')
@if(Session::get('success'))
<div class="alert alert-success" role="alert" id="success-alert">
    <strong>{{Session::get('success')}} </strong>
</div>
@endif
@if(Session::get('danger'))
<div class="alert alert-danger" role="alert">
    <strong>{{Session::get('danger')}} </strong>
</div>
@endif
<form action="{{isset(request()->id) ? route('updateProduct',$prodRecord->id) : route('addNewProduct') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="pname">Product Name</label>
        <input type="text" class="form-control" name="pname" placeholder="Enter Product Name" value="{{ isset(request()->id) ? $prodRecord->pname : '' }}" autofocus>
        @error('pname')
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="c_name">Category Name</label>
        <select class="js-example-basic-single form-control" name="c_id">
            @foreach ($cat_record as $val)
            <option value="{{$val->id}}">{{ $val->c_name }}</option>
            @endforeach
        </select>
        @error('c_id')
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="p_price">Product Price</label>
        <input type="number" id="p_price" class="form-control" name="p_price" placeholder="Enter Product Price" value="{{ isset(request()->id) ? $prodRecord->p_price : '' }}">
        @error('p_price')
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="p_saleprice">Product Sale Price</label>
        <input type="number" class="form-control" id="p_saleprice" name="p_saleprice" placeholder="Enter Product Sale Price" value="{{ isset(request()->id) ? $prodRecord->p_saleprice : '' }}">
        @error('p_saleprice')
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="p_quantity">Product Quantity</label>
        <input type="number" class="form-control" id="p_quantity" name="p_quantity" placeholder="Enter Product Quantity" value="{{ isset(request()->id) ? $prodRecord->p_quantity : '' }}">
        @error('quantity')
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="p_order">Product Order</label>
        <input type="number" class="form-control" name="p_order" placeholder="Enter Product Order" value="{{ isset(request()->id) ? $prodRecord->p_order : '' }}">
        @error('p_order')
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="pstatus">Status</label>
        <select class="form-control" name="status">
            <option value="">--Select Status--</option>
            <option value="Active" {{ isset(request()->id) ? ($prodRecord->status == 'Active' ? 'selected' : '') : '' }}>Active
            </option>
            <option value="Inactive" {{ isset(request()->id) ? ($prodRecord->status == 'Inactive' ? 'selected' : '') : '' }}>Inactive
            </option>
        </select>
        @error('status')
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    @if(isset(request()->id))
    <label for="p_img">Product Image</label>
    <input type="file" class="form-control" name="p_img[]" multiple>
    @foreach($prodImageRecord as $val)
    @if($val->status == "Active")

    <img src="{{asset('asset/img/product/'.$val->p_img)}}" class="border border-primary" width="200px" height="200px">

    @else
    <img src="{{asset('asset/img/product/'.$val->p_img)}}" width="200px" height="200px">
    <a href="{{route('prodActiveInactive', ['p_id'=> $val->p_id, 'id' => $val->id])}}" class="btn btn-primary">Active</a>
    <a href="{{route('prodDelete', ['id' => $val->id])}}" class="btn btn-danger">Delete</a>
    @endif
    @endforeach
    @error('p_img')
    <div class="text-danger">{{ $message }}</div>
    @enderror
    @else
    <div class="form-group">
        <label for="p_img">Product Image</label>
        <input type="file" class="form-control" name="p_img[]" multiple>
        @error('p_img')
        <div class="text-danger">{{ $message }}</div>
        @enderror
        @endif
    </div>

    @if (request()->id)
    <input type="submit" name="update" class="btn btn-primary my-3" value="Update Product">
    @else
    <input type="submit" name="submit" class="btn btn-primary my-3" value="Add Product">
    @endif
</form>
<script src="{{asset('asset/js/jquery.min.js')}}"></script>
<link href="{{asset('asset/css/select2.min.css')}}" rel="stylesheet" />
<script src="{{asset('asset/js/select2.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $(".alert").fadeTo(2000, 2000).slideUp(2000, function() {
            $(".alert").slideUp(5000);
        });
        $('.js-example-basic-single').select2();
    });
</script>

@endsection