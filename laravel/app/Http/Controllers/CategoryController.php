<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {
        // this method is Eloquent method to get all record from category table
        $category = Category::all();
        return view('category.c_listing', ['category' => $category]);
    }

    public function create()
    {
        return view('category.c_add');
    }

    public function store(Request $request)
    {
        $catValidate = $request->validate([
            'c_name' => 'required|unique:categorys',
            'c_file' => 'image|mimes:jpeg,png,jpg|max:2048',
            'order' => 'required',
            'status' => 'required',
        ]);

        $data = $request->all();
        $c_insert = new Category();
        $c_insert->c_name = $data['c_name'];
        $fileName = time() . '.' . $request->c_file->extension();
        $request->c_file->move(public_path('/asset/img/category'), $fileName);
        $c_insert->order = $data['order'];
        $c_insert->c_file = $fileName;
        $c_insert->status = $data['status'];
        $c_insert->save();
        return redirect('categoryListing')->with('cat_success', 'Category Added Sucessfully');
    }

    public function edit($id)
    {
        $c_edit = Category::find($id);
        return view('category.c_add', ['category' => $c_edit]);
    }
    public function update(Request $request, $id)
    {
        $image = $request->c_file;
        $cat_validate = $request->validate([
            'c_name' => 'required',
            'order' => 'required',
            'status' => 'required',
        ]);

        $status = $request->input('status');
        $order = $request->input('order');
        $c_name = $request->input('c_name');

        if ($image != null) {
            $fileName = time() . '.' . $request->c_file->extension();
            $request->c_file->move(public_path('/asset/img/category'), $fileName);
            Category::where('id', $id)->update([
                'c_name' => $c_name,
                'order' => $order,
                'c_file' => $fileName,
                'status' => $status,
            ]);
        } else {
            Category::where('id', $id)->update([
                'c_name' => $c_name,
                'order' => $order,
                'status' => $status,
            ]);
        }
        return redirect('categoryListing')->with('cat_success', 'Category Update Successfylly');
    }
    public function destroy($id)
    {
        $cat_delete = Category::find($id);
        unlink(public_path('asset/img/category/' . $cat_delete->c_file));
        $cat_delete->delete();
        return redirect('categoryListing')->with('cat_danger', 'Category Deleted Successfully');
    }

    public function catShow(Request $request)
    {
        if ($request->ajax()) {
            $cat_record = Category::where('c_name', 'LIKE', '%' . $request->catname . "%")->get();
            if ($cat_record) {
                $output = "";
                $img = url('asset/img/category/');
                foreach ($cat_record as $key => $product) {
                    $output .= '<tr>' .
                        '<td>' . $product->id . '</td>' .
                        '<td>' . $product->c_name . '</td>' .
                        '<td> <img src=' . $img . "/" . $product->c_file . ' width=70px height=70px></td>' .
                        '<td>' . $product->order . '</td>' .
                        '<td>' . $product->status . '</td>' .
                        '<td>' . $product->created_at->format('d/m/Y') . '</td>' .
                        '<td>' . $product->updated_at->format('d/m/Y') . '</td>' .
                        '<td> <a class ="btn btn-success" href=catEdit/' . $product->id . '> Edit</a></td>' .
                        '<td> <a class ="btn btn-danger" href=catDelete/' . $product->id . '>Delete</a></td>' .

                        '</tr>';
                }
                return Response($output);
            }
        }
    }
}
