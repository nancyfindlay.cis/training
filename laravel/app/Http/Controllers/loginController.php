<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class loginController extends Controller
{
    public function index()
    {
        return view('login');
    }
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $auth = Auth::guard('admin');
        if ($auth->attempt($credentials)) {   
            return view('dashboard');
        }
        return redirect()->route('login')->with('alert-info', 'Login Fail, please check email or password');
    }
    public function logout()
    {
        Auth()->guard('admin')->logout();
        return view('login');
    }
}
